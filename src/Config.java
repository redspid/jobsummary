package com.redspid.office;

import java.io.File;
import java.io.Reader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.IOException;
import java.util.Date;

class Config {
	synchronized public static Config getInstance() {
		if ( instance==null ) instance = new Config();

		return instance;
	}

	public Config() {
		File currentPath = new File(".");
		String absolutePath = currentPath.getAbsolutePath();
		int offset = absolutePath.lastIndexOf( File.separator );

		this.yearMonth = absolutePath.substring(offset-8, offset-2);

		try {
			templateString = readWholeFile( Config.class.getResourceAsStream("/template.html") );

		} catch( Exception e ) { e.printStackTrace();  }
	}

	// Begin get arguments --------------------{

	public String getTemplateString() { return templateString; }

	public String getBriefDirName()
	{ return yearMonth; }
	//{ return String.format("%tY%<tm", new Date()); }

	public String getYear() { return yearMonth.substring(0,4); }
	public String getMonth() { return yearMonth.substring(4); }

	public String getRoutineDir() { return "routine"; }

	public String getProfileName() { return "profile.txt"; }

	public String getSummaryDocName() { return "JobSummary.html"; }

	// End get arguments --------------------}

	private String readWholeFile( InputStream resIn ) throws IOException {
		StringBuilder sb = new StringBuilder();

		Reader in = new InputStreamReader( resIn );

		int size=1024;
		int len=0;
		char [] c = new char[size];

		while ( ( len=in.read(c, 0, size) ) != -1 ) sb.append(c,0,len);

		return sb.toString();
	}

	private static Config instance;
	private String profileName;
	private String templateString;
	private String yearMonth;

}
