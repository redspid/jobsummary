package com.redspid.office;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Scanner;
import java.io.IOException;

import biz.source_code.miniTemplator.MiniTemplator;

public class JobSummary {
	public static void main( String[] args ) throws Exception {

		Config cfg = Config.getInstance();

		File curDir = new File(".");
		File [] workDirs = curDir.listFiles( new DirFilter() );

		// mkdir yyyymm here
		File briefDir = new File( cfg.getBriefDirName() );
		briefDir.mkdir();

		// create a MiniTemplator here
		MiniTemplator.TemplateSpecification tplSpec =
			new MiniTemplator.TemplateSpecification();
		tplSpec.templateText = cfg.getTemplateString();
		MiniTemplator tpl = new MiniTemplator( tplSpec );

		// global variable set
		tpl.setVariable( "year", cfg.getYear() );
		tpl.setVariable( "month", cfg.getMonth() );

		// then deal with every work directory
		int sn=1; // sn with item
		for (File workDir : workDirs) {
			// mkdir in yyyymm/
			File workDirCopy = mkdirCopy(workDir, briefDir);

			// copy files to yyyymm/workDir/
			for ( File file : workDir.listFiles(new ContentFilter()) )
				fileCopy(file, workDirCopy);

			// read wordDir/profile.txt and parse it
			WorkDirParser dParser = new WorkDirParser(workDir.getName());

			// set template
			tpl.setVariable( "consuming", dParser.getWorkFlag() );
			tpl.setVariable( "sn", sn );
			tpl.setVariable( "dirName", cfg.getBriefDirName() );
			tpl.setVariable( "title", workDir.getName() );
			tpl.setVariable( "date", dParser.getDate() );

			for (String paragraph : dParser.getContents() ) {
				tpl.setVariable( "itemContent", paragraph );
				tpl.addBlock("itemContents");
			}

			tpl.setVariable( "contact", dParser.getContact() );
			tpl.addBlock("item");

			++sn;
		}

		// read routine/profile.txt and set template
		RoutineParser rParser = new RoutineParser();

		for ( String[] rt : rParser.getContents() ) {
			tpl.setVariable( "consuming",  rt[0] );
			tpl.setVariable( "routineItemContent",  rt[1] );
			tpl.addBlock( "routineItem" );
		}


		// write yyyymm/JobSummary.html with miniTemplator
		tpl.generateOutput( cfg.getSummaryDocName() );

	}

	private static File mkdirCopy(File dir, File parentDir) {
		String dirName = parentDir.getAbsolutePath() +
			File.separator + dir.getName();
		File newDir = new File(dirName);
		newDir.mkdir();
		return newDir;
	}

	private static void fileCopy(File source, File dir) throws IOException {
		File destination =
			new File( dir.getPath() + File.separator + source.getName() );

		byte [] buf = new byte[1024];
		int len=0;
		FileInputStream in = new FileInputStream( source );
		FileOutputStream out = new FileOutputStream( destination );

		while ( (len=in.read(buf)) != -1 ) out.write(buf, 0, len);

		in.close();
		out.close();
	}
}
