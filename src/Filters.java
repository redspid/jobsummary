package com.redspid.office;

import java.io.File;
import java.io.FileFilter;
import java.io.FilenameFilter;

class DirFilter implements FileFilter {

	public boolean accept(File pathname) {
		Config cfg = Config.getInstance();

		// should be a directory
		if ( pathname.isDirectory() ) {

			// directory name not "routine", it's configured
			if ( pathname.getName().equals( cfg.getRoutineDir() ) ) return false;
			else return new File(pathname.getName() + File.separator + cfg.getProfileName()).exists();
		}

		return false;
	}
}

class ContentFilter implements FilenameFilter {
	public boolean accept(File dir, String fileName) {

		// extend name include *.jpg *.jpeg *.png *.doc
		// or file name is "profile.txt"
		if ( fileName.matches(".+\\.(jpg|jpeg|png|doc)") || fileName.equals( Config.getInstance().getProfileName()) )
			return true;

		return false;
	}
}
