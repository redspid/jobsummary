package com.redspid.office;

import java.io.File;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.LinkedList;

/**
  * read workDir/profile.txt and parse it
  */
class WorkDirParser {
	public WorkDirParser(String workDir) throws IOException {
		curDirProfile = new File( workDir + File.separator +
				Config.getInstance().getProfileName() );

		contents = new ArrayList<String>();

		parse( curDirProfile );
	}

	private void parse( File aFile ) throws IOException {
		LinkedList<String> fileContent = new LinkedList<String>();


		BufferedReader in = new BufferedReader(
				new FileReader( aFile ) );

		String line;
		while ( (line=in.readLine()) != null )
			fileContent.addLast( line );

		/**
		  * parse LinkedList and set private variable
		  */
		// delete all "blank" at the last of list
		while ( true ) {
			line = fileContent.peekLast();

			if ( line.equals("") )
				fileContent.pollLast();
			else break;
		}

		// parse first line
		line = fileContent.pollFirst();
		int offset=0;
		if ( (offset=line.indexOf(" ")) != -1 ) {
			date = line.substring(0,offset);

			String workFlagg = line.substring(offset+1);
			if ( workFlagg.equals("e") )
				workFlag="extra";
			else if ( workFlagg.equals("h") )
				workFlag="hard";
			else workFlag="normal";
		} else {
			date = line;
			workFlag="normal";
		}

		fileContent.pollFirst(); // delete second blank line.

		// parse last line
		line = fileContent.peekLast();
		if ( line.startsWith("-") ) {
			contact = line.substring(1);
			fileContent.pollLast(); // delete last contact line.
		}

		// assembel content
		StringBuilder sb = new StringBuilder();
		for ( String c : fileContent ) {
			if ( c.equals("") ) {
				if ( sb.length() == 0 ) continue;

				contents.add( sb.toString() );
				sb.delete(0, sb.length());
			} else sb.append(c);
		}

		if ( sb.length()>0 ) contents.add( sb.toString() );

	}

	public String getDate() { return date; }
	public String getWorkFlag() { return workFlag; }
	public List<String> getContents() { return contents; };
	public String getContact() { return contact; };

	private File curDirProfile;
	private String date;
	private String workFlag;
	private List<String> contents=null;
	private String contact="";
}


/**
  * read routine/profile.txt and parse it 
  */
class RoutineParser {
	public RoutineParser() throws IOException {
		File routine = new File("./routine/profile.txt");
		contents = new ArrayList<String[]>();

		parse( routine );
	}

	public List<String[]> getContents() { return contents; }

	private void parse( File aFile ) throws IOException {
		BufferedReader in = new BufferedReader(
				new FileReader( aFile ) );

		String line;
		String flag;
		String itemContent;
		while ( (line=in.readLine()) != null ) {
			if ( line.equals("") ) continue;

			flag = line.substring(19,22);
			if ( flag.equals(" e ") ) {
				flag="extra";
				itemContent = line.substring(0,19) + line.substring(21);
			} else if ( flag.equals(" h ") ) {
				flag="hard";
				itemContent = line.substring(0,19) + line.substring(21);
			} else {
				flag="normal";
				itemContent = line;
			}

			String[] itm = {flag, itemContent};
			contents.add( itm );
		}
	}

	private List<String[]> contents; //{"flag", "itemContent"}
}

